<?php
/*
Plugin Name: GF Upload to Email Attachment
Plugin URI: http://wpcms.ninja/
Description: Configure Gravity Form notifications to delete uploaded file(s) and/or zip them before email.
Author: Greg Whitehead and Will Skora
Author URI: http://wpcms.ninja
Version: 3.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

add_filter( 'gform_notification', 'GFUEA_custom_notification_attachments', 10, 3 );
function GFUEA_custom_notification_attachments( $notification, $form, $entry ) {
	global $gf_delete_files;
	$count = 0;
	$log   = 'rw_notification_attachments() - ';
	GFCommon::log_debug( $log . 'starting.' );
	$last5                  = substr( $notification['name'], -5 );
	$last7                  = substr( $notification['name'], -7 );
	$attach_upload_to_email = (bool) rgar( $notification, 'enableAttachments' );
	$zip_attachment         = (bool) rgar( $notification, 'gfu_zip_attachment' );
	$gf_delete_files        = (bool) rgar( $notification, 'gfu_delete_files' );

	$clean_files = false;

	if ( ( $last5 == 'GFUEA' || $last7 == 'GFUEANZ' ) || $attach_upload_to_email == true ) {
		if ( $last7 == 'GFUEANZ' || $zip_attachment == false ) {
			$attemptzip = false;
		} else {
			$attemptzip = true;
		}
		$fileupload_fields = GFCommon::get_fields_by_type( $form, array( 'fileupload' ) );
		if ( ! is_array( $fileupload_fields ) ) {
			return $notification;
		}
		$attachments = array();
		$upload_root = RGFormsModel::get_upload_root();
		foreach ( $fileupload_fields as $field ) {
			$url = $entry[ $field['id'] ];
			if ( empty( $url ) ) {
				continue;
			} elseif ( $field['multipleFiles'] ) {
				$uploaded_files = json_decode( stripslashes( $url ), true );
				$zip            = new ZipArchive();
				$filename       = $upload_root . '/uploaded_files' . $entry['id'] . '.zip';
				if ( $zip->open( $filename, ZipArchive::CREATE ) !== true || $attemptzip == false ) {
					foreach ( $uploaded_files as $uploaded_file ) {
						$attachment = preg_replace( '|^(.*?)/gravity_forms/|', $upload_root, $uploaded_file );
						GFCommon::log_debug( $log . 'attaching the file: ' . print_r( $attachment, true ) );
						$attachments[] = $attachment;
						$count++;
					}
				} else {
					foreach ( $uploaded_files as $uploaded_file ) {
						$attachment = preg_replace( '|^(.*?)/gravity_forms/|', $upload_root, $uploaded_file );
						GFCommon::log_debug( $log . 'attaching the file: ' . print_r( $attachment, true ) );
						$new_filename = substr( $attachment, strrpos( $attachment, '/' ) + 1 );
						$zip->addFile( $attachment, $new_filename );
						$count++;
					}
					$zip->close();
					$attachments[] = $filename;
					//add_filter( 'gform_confirmation', 'gfuea_clean_zips', 10, 4 );
					$clean_files = true;
				}
			} else {
				$count++;
				$attachment = preg_replace( '|^(.*?)/gravity_forms/|', $upload_root, $url );
				GFCommon::log_debug( $log . 'attaching the file: ' . print_r( $attachment, true ) );
				$attachments[] = $attachment;
			}
		}
		if ( $clean_files || $gf_delete_files == true ) {
			add_filter( 'gform_confirmation', 'gfuea_clean_zips', 10, 4 );
		}
		$notification['attachments'] = $attachments;
	}
	GFCommon::log_debug( $log . 'stopping.' );
	return $notification;
}

function gfuea_clean_zips( $confirmation, $form, $entry, $ajax ) {
	global $gf_delete_files;
	$upload_root = RGFormsModel::get_upload_root();
	$filename    = $upload_root . '/uploaded_files' . $entry['id'] . '.zip';
	if ( is_file( $filename ) ) {
		unlink( $filename );
	}
	if ( $gf_delete_files == true ) {
		//delete all files that were uploaded

		$attachments_to_delete = array();

		$fileupload_fields = GFCommon::get_fields_by_type( $form, array( 'fileupload' ) );
		if ( is_array( $fileupload_fields ) ) {
			$upload_root = RGFormsModel::get_upload_root();
			foreach ( $fileupload_fields as $field ) {
				$url = $entry[ $field['id'] ];
				if ( empty( $url ) ) {
					continue;
				} elseif ( $field['multipleFiles'] ) {
					$uploaded_files = json_decode( stripslashes( $url ), true );
					if ( is_array( $uploaded_files ) ) {
						foreach ( $uploaded_files as $uploaded_file ) {
							$attachments_to_delete[] = preg_replace( '|^(.*?)/gravity_forms/|', $upload_root, $uploaded_file );
						}
					}
				} else {
					$attachments_to_delete[] = preg_replace( '|^(.*?)/gravity_forms/|', $upload_root, $url );
				}
			}
		}

		if ( is_array( $attachments_to_delete ) && count( $attachments_to_delete ) > 0 ) {
			foreach ( $attachments_to_delete as $attachment ) {
				unlink( $attachment );
			}
		}
	}

	return $confirmation;
}

add_filter( 'gform_notification_settings_fields', 'gf_upload_notification_setting', 10, 3 );

function gf_upload_notification_setting( $fields, $notification, $form ) {
	$fields[0]['fields'][] =
	array(
		'name'    => 'gfu_array',
		'label'   => esc_html__( 'Gravity Forms Upload - Extra Options', 'gravityforms' ),
		'type'    => 'checkbox',
		'choices' => array(
			array(
				'label' => esc_html__( 'Delete attachments from uploads folder after being sent', 'gravityforms' ),
				'name'  => 'gfu_delete_files',
			),
			array(
				'label' => esc_html__( 'Zip attachment before being sent', 'gravityforms' ),
				'name'  => 'gfu_zip_attachment',
			),
		),
	);
	return $fields;
}

//
add_filter( 'gform_pre_notification_save', 'gf_upload_notification_save', 40, 2 );
function gf_upload_notification_save( $notification, $form ) {

	$gf_zip                             = rgpost( 'gfu_zip_attachment' );
	$gf_delete                          = rgpost( 'gfu_delete_files' );
	$notification['gfu_zip_attachment'] = (bool) ( $gf_zip == 'on' ? $gf_zip : 'no' );
	// $notification['gfu_delete_files'] = ( $gf_delete == 'on' ? $gf_delete : 'no' );
	$notification['gfu_delete_files'] = (bool) ( $gf_delete == 'on' ? $gf_delete : 'no' );
	return $notification;
}

function gf_upload_sanitize_file_name( $filename ) {

	$sanitized_filename = remove_accents( $filename ); // Convert to ASCII

	// Standard replacements
	$invalid            = array(
		' '   => '-',
		'%20' => '-',
		'_'   => '-',
	);
	$sanitized_filename = str_replace( array_keys( $invalid ), array_values( $invalid ), $sanitized_filename );

	$sanitized_filename = preg_replace( '/[^A-Za-z0-9-\. ]/', '', $sanitized_filename ); // Remove all non-alphanumeric except .
	$sanitized_filename = preg_replace( '/\.(?=.*\.)/', '', $sanitized_filename ); // Remove all but last .
	$sanitized_filename = preg_replace( '/-+/', '-', $sanitized_filename ); // Replace any more than one - in a row
	$sanitized_filename = str_replace( '-.', '.', $sanitized_filename ); // Remove last - if at the end
	$sanitized_filename = strtolower( $sanitized_filename ); // Lowercase

	return $sanitized_filename;
}
add_filter( 'sanitize_file_name', 'gf_upload_sanitize_file_name', 10, 1 );
