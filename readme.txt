=== GF Upload to Email Attachment ===
Contributors: billiardgreg, skorasaurus
Donate link: http://wpcms.ninja/
Tags: gravityforms
Requires at least: 5.0
Tested up to: 6.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Configure Gravity Form notifications to delete uploaded file(s) immediately after the files are emailed.

== Description ==

Gravity Forms was built to store all uploaded files to the server.
This plugin deletes the files from the server immediately after Gravity Forms emails the files to the designated recipient. 

Should work with both single and multiple upload boxes as well as multiple notifications.  I left the old GFUEA and GFUEANZ designations in the name for people still using it.

== Frequently Asked Questions ==

= Where can I get answers to questions? =

Support is not guaranteed in any manner; I will try to respond in a reasonable manner. Email skorasaurus at gmail dot com

== Installation ==

Install plugin and activate.

Note: 
The checkboxes toggle function is broken; to disable the plugin's functionality, disable the the plugin. (To be fixed). 


== Screenshots ==

1. No Screenshot

== Changelog ==

= 3.0 =

* Supports Gravity Forms 2.5+ (last tested with Gravity Forms 2.6.8);
* Requires Gravity Forms 2.5 or later (older versions no longer work)
* Now uses GravityForms' Built-in Email Attachment feature
* The zipping attachment feature NO LONGER works (and will always be toggled on, for some reason)

= 2.2 =
* Slight Modification to wording.

= 2.1.2 =
* Confirmed zip functionality and delete functionality.

= 2.0 =
* Decided recent change signified a good step forward to making it 2.0. Also cleaned up code.

= 1.3.1 =
* Added customization through checkboxes instead of through name modification.
* Kept name modification active to keep old implementation working.

= 1.3 =
* Updated readme.

= 1.2 =
* Added no zip addition to multifile upload.

= 1.1 =
* Added zip functionality to multifile upload.

= 1.0 =
* Updated description and changed to stable version 1.0

= .1 =
* Initial Release of Plugin

== Upgrade Notice ==

= 3.0 =
* Supports Gravity Forms 2.5 (last tested with Gravity Forms 2.5, beta 3.1)

= 2.2 =
* Slight Modification to wording.

= 2.1.2 =
* Confirmed zip functionality and delete functionality.

= 2.0 =
* Went through cleaning up code.

= 1.3.1 =
* Modified system to work with checkboxes in notification area instead of modifying the name of the notification.  Made it so plugin still works with name modification.

= 1.3 =
* Updated readme.

= 1.2 =
* Added no zip addition to multifile upload.

= 1.1 =
* Added zip functionality to multifile upload.

= .1 =
* Initial Release of Plugin
